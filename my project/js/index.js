function removeR() {
	let img = document.querySelectorAll( 'img' );
	img.forEach( function( item, i ) {
		if( img[ i ].alt === 'www.000webhost.com' ){
			img[ i ].style.display = 'none';
		}
	});		
}
removeR();
///////////////////////////




let chooise = document.getElementById( 'chooise' );
let watch = document.getElementById( 'watch' );
	let rezult = document.getElementById( 'rezult' );

let buttons = generator_forms.querySelectorAll( '.chooise' );
let divs = generator_forms.querySelectorAll( '.second_chooise' );
let clears = generator_forms.querySelectorAll( '.clear' );
let sets_buttn = generator_forms.querySelectorAll( '.set' );
let remove_from_form = generator_forms.querySelectorAll( '.remove_from_form' );



////////////////////
// навешивает обработчики
sets_buttn.forEach( function( item, i ) {
	sets_buttn[ i ].addEventListener( 'click', push_to_watch );
});
buttons.forEach( function( item, i ) {
	buttons[ i ].addEventListener( 'click', create_markup );
});

clears.forEach( function( item, i ) {
	clears[ i ].addEventListener( 'click', clearParent );
});
remove_from_form.forEach( function( item, i ) {
	remove_from_form[ i ].addEventListener( 'click', remoFForm );
});
/////////////////////
/////////////////////
/////////////////////
/////////////////////
/////////////////////

// создает хтмл разметку по заказу клиента
function createHTML__for_user_form( required , tag, value, placeholder, text, radio_value, checkbox_value, range_value, radio_name, max_length, colorDef, dateDef ) {
//										1		2	  3         4		  5		   6			 7				8			9			10
	let unswer = document.createElement( tag );
	if( tag === 'textarea' || tag === 'input' ){
			if( value !== undefined ){
				unswer.value = value;
			}
			if( placeholder !== undefined ){
				unswer.placeholder = placeholder;
			}
			if( max_length !== undefined && typeof max_length === 'number' && max_length > 0 ){
				unswer.setAttribute( 'maxLength', max_length );
			}
	} else if( tag === 'p' ){
			if( text != undefined && text != '' && text != null ){
				unswer.innerText = text;
			} else {
				unswer.innerText = 'Lorem ipsum dolor sit amet...';
			}
			tag = 'text';
	} else if( tag === 'radio' ){
			unswer = document.createElement( 'input' );
			unswer.type = tag;
			unswer.name = radio_name;
			if( radio_value !== undefined ){
				let support_span = document.createElement( 'span' );
				support_span.classList.add( 'support_span' );
				support_span.innerText = radio_value;
				rezult.appendChild( support_span );
			}
	} else if( tag === 'checkbox' ){
			unswer = document.createElement( 'input' );
			unswer.type = tag;
			if( radio_value !== undefined ){
				let support_span = document.createElement( 'span' );
				support_span.classList.add( 'support_span' );
				support_span.innerText = checkbox_value;
				rezult.appendChild( support_span );
			}
	} else if( tag === 'date' || tag === 'submit' || tag === 'reset' || tag === 'color' ){
			unswer = document.createElement( 'input' );
			unswer.type = tag;
			if( tag === 'color' ){
				unswer.value = colorDef;
			} else if( tag === 'date'){
				tag.value = Number( dateDef );
			}
	} else if( tag === 'password' || tag === 'email' ){
			unswer = document.createElement( 'input' );
			unswer.type = tag;
			if( placeholder !== undefined ){
				unswer.placeholder = placeholder;
			}

	} else if( tag === 'range' ){
			unswer = document.createElement( 'input' );
			unswer.type = tag;
			if( range_value !== undefined ){
				unswer.value = range_value;
			}
	}
	if( required ){
		console.log( 'required' );
		unswer.setAttribute( 'required', "" );
	}
	unswer.classList.add( 'js_'+tag );
	// console.log( 'js_'+tag );
	rezult.appendChild( unswer );
	rezult.appendChild( document.createElement( 'br' ) );
}


// возвращает позицию елемента в массиве
function getPosition( item, arr ) {
	for( let i = 0; i < arr.length; i++ ){
		if ( item.innerHTML === arr[ i ].innerHTML ){
			return i;
		}
	}
}
function clear( arr ) {
	arr.forEach( function( item, i ) {
		arr[ i ].value = null;
	});
}

// собирает данные с конструктора и переносит их в rezult
function push_to_watch() {
	let parent = this.parentNode;
	let inputs = parent.querySelectorAll( '.input' );	
	let date_info = this.getAttribute( 'data-info' );

	if( date_info === 'text' ){
		createHTML__for_user_form( null, 'p', null, null, inputs[ 0 ].value );
	} else if( date_info === 'input' ){
		createHTML__for_user_form( inputs[ 2 ].checked, 'input', null, inputs[ 0 ].value, null,null,null,null, null, Number( inputs[ 1 ].value ) );
	} else if( date_info === 'textarea' ){
		createHTML__for_user_form( null, 'textarea', null, inputs[ 0 ].value );
	} else if( date_info === 'checkbox' ){
		createHTML__for_user_form( null, 'checkbox', null, null, null, null, inputs[ 0 ].value );
	} else if( date_info === 'radio' ){
		createHTML__for_user_form( null, 'radio', null, null, null, inputs[ 0 ].value, null, null, inputs[ 1 ].value );
	} else if( date_info === 'date' ){
		createHTML__for_user_form( null, 'date', null, null, null, null, null, null, null, null, null, new Date( inputs[ 0 ].value ) );
	} else if( date_info === 'color' ){ 
		createHTML__for_user_form( false, 'color', null, null, null, null, null, null, null, null, inputs[ 0 ].value );
	} else if( date_info === 'password' ){
		createHTML__for_user_form( false, 'password', null, inputs[ 0 ].value );
	} else if( date_info === 'reset' || date_info === 'submit' ){
		createHTML__for_user_form( null, date_info );
	} else if( date_info === 'email' ){
		createHTML__for_user_form( null, 'email', null, inputs[ 0 ].value );
	} else if( date_info === 'range' ){
		createHTML__for_user_form( null, 'range', null, null, null, null, null, Number( inputs[ 0 ].value ) );
	} else {
		console.error( "date_info не прошла валидацию и скрипт не выполнился" );
	}
	clear( inputs );
}

function remoFForm( arr ) {
	let remove_name = this.getAttribute( 'data-ramove' );
	let jsclasses = rezult.querySelectorAll( '.js_'+remove_name );

	let br = rezult.querySelectorAll( 'br' );

	let position = jsclasses.length;

	if( this.parentNode.querySelector( '.remove_index' ).value != '' ){
		position = this.parentNode.querySelector( '.remove_index' ).value /*- 1*/;
	}	
	if( jsclasses[ position - 1 ] !== undefined && br[ position -1 ] !== undefined ){
		rezult.removeChild( jsclasses[ position - 1 ] );
		rezult.removeChild( br[ position - 1 ] )	
	}
}


// убирает у родителя класс none
function unselected( arr ) {
	arr.forEach( function( item, i ) {
		arr[ i ].classList.add( 'none' );
	});
}

// чистит все поля с классом .input
function clearParent() {
	let inputs = this.parentNode.querySelectorAll( '.input' );
	inputs.forEach( function( item, i ) {
		inputs[ i ].value = null;
	});
}

// по онклику убирает у родителя класс none
function create_markup() {
	if( this.classList.contains( 'sel' ) ){
		let position = getPosition( this, buttons );
		unselected( divs, 'none' );
		divs[ position ].classList.toggle( 'sel' );

		this.classList.toggle( 'sel' )
		this.style.background = '#fff';
	} else {
		buttons.forEach( function( item, i ) {
			buttons[ i ].classList.remove( 'sel' );
		});
		this.classList.add( 'sel' );

		let position = getPosition( this, buttons );
		unselected( divs, 'none' );
		divs[ position ].classList.toggle( 'none' );

	}
}







