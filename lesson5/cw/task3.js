/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода

    2. Обьект должен иметь пару свойств (Имя, порода, status)
    
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

// let Dog = constructor( 'Bob', 'Doberman' );

function constructor( name, breed ) {
    let dog = {};
    dog.name = name;
    dog.breed = breed;
    dog.status = "Собака есть";
    dog.go = function() {
        dog.status = "Собака бежит";
    },
    dog.have =  function() {
       dog.status = "Собака есть"; 
    },
    dog.show = function() {
        for ( let key in dog ){
            if ( typeof dog[ key ] === 'string' )
            console.log( key + ' - '+dog[ key ] );
        }
    }
    return dog;
}


let Dog = constructor( 'Bob', 'Doberman' );

Dog.show();






















/*
 ___    ___          __          ________    ________   ____      ____   ___
|   |  |   |        /  \        |    _   \  |    _   \  \   \    /   /  |   |
|   |  |   |       / /\ \       |   | |   | |   | |   |  \   \  /   /   |   |
|   |__|   |      / /  \ \      |   | |   | |   | |   |   \   \/   /    |   |
|    __    |     / /____\ \     |   |_|   | |   |_|   |    \      /     |   |
|   |  |   |    /  ______  \    |    ____/  |    ____/      \    /      |   |
|   |  |   |   /  /      \  \   |   |       |   |           /   /       |___|
|   |  |   |  /  /        \  \  |   |       |   |          /   /         ___
|___|  |___| /__/          \__\ |___|       |___|         /___/         |___|

*/