
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary



  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>

    <button></button>
  </form>


    <input />

  -> '{"name" : "123123", "age": 15, "password": "*****" }'

*/

let name = document.getElementById( 'name' );
let age = document.getElementById( 'age' );
let password = document.getElementById( 'password' );
let in__put = document.getElementById( 'in__put' );
let button = document.getElementById( 'button' );
let json__on = document.getElementById( 'json__on' );
button.onclick = function( e ) {
  let reg__info = {
    name: name.value,
    age: age.value,
    password: password.value
  };
  console.log( 'json - ',JSON.stringify( reg__info ) );
  in__put.value = JSON.stringify( reg__info );
}
json__on.onclick = function ( e ) {
  console.log( 'obj - ',JSON.parse( in__put.value ) );
}