/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  
    http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button			 |
    2.| CompName | 2000$   | 20/10/2019                | button			 |
    3.| CompName | 2000$   | button                    | button          |
    4.| CompName | 2000$   | button                    | button          |
    5.| CompName | 2000$   | button                    | button          |

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/




async function get__respose( http ) {
	let respose = await fetch( http );
	let unswer = await respose.json();
	return unswer;
}
const temp = get__respose( 'http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2' );
temp.then( res => {

////////////
console.log( res );

let show = function () {
    let id = this.id[ this.id.length - 1 ];
    console.log( id );
    if( this.id[ 2 ] === 't'){
        document.getElementById( 'sppn'+id ).style.display = 'inline-block';
    } else {
        document.getElementById( 'spn'+id ).style.display = 'inline-block';
    }
    
    this.style.display = 'none';
}

let createTable = function ( howManyTr, howManyTd, arr ) {

function reg( regData, count ) {
        return `<span style="display:none" id="spn${count}">${regData}</span><button id="btn${count}">Show date</button>`;
        /*
        rezult:
        `<div style="display: block">
            <span style="display:none" id="spn${count}>${regData}</span>
            <button id="btn${count}"></button>
        </div>`;
        */
    }
    function addr( adress, count ) {
        return `<span style="display:none; font-size: 12px;" id="sppn${count}">${adress.country}. ${adress.state}, ${adress.city}. ${adress.street} , ${adress.zip}</span><button id="bttn${count}">Show adress</button>`;
    }

let count = 0;
let table = '<table border="1">';
for ( let i = 0; i < howManyTd; i++ ){
    table += '<tr>';
    if( i === 0 ){
        table += `<td>Company Name</td>`;
        table += `<td>Balanse</td>`;
        table += `<td>Date of regisered</td>`;
        table += `<td>Adress</td>`;
    } else {
        for ( let j = 0; j < howManyTr; j++ ){
            table += '<td>';
                switch( j ){
                    case 0 : table += '<b>'+res[ i ].company+'</b>'; break;
                    case 1 : table += res[ i ].balance; break;
                    case 2 : table += reg( res[ i ].registered, count ); break;
                    case 3 : table += addr( res[ i ].address, count ); break;
                    default : console.error( '!' );
                }
            table += '</td>';
        }
        table += '</tr>';
    }
    count++;
}
table += '</table>';
// console.log( table );


let rezult = document.getElementById( 'rezultTable' );
rezult.innerHTML = table;
/////
let buttons = document.querySelectorAll( 'button' );
for( let b = 0; b < buttons.length; b++){
    buttons[ b ].addEventListener( 'click', show );
}
////
}
createTable( 4, 7, res );


});