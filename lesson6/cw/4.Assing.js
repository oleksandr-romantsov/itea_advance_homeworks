let f__key = document.getElementById( 'f__key' );    //
let f__value = document.getElementById( 'f__value' );//
let f__fsend = document.getElementById( 'f__fsend' );//
let f__createdObj = document.getElementById( 'f__createdObj' );
///////////////////////////////////////////////////////
let s__key = document.getElementById( 's__key' );    //
let s__value = document.getElementById( 's__value' );//
let s__fsend = document.getElementById( 's__fsend' );//
let s__createdObj = document.getElementById( 's__createdObj' );
///////////////////////////////////////////////////////
let result = document.getElementById( 'result' );
let combine = document.getElementById( 'combine' );
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

let firstObj = {
	id:123,
    b:'test',
    demo:'test2',
    show: 'worked!'
};
let secondObj = {	
	id:123,
    b:'test',
    demo:'test2'
};

function add_to_obj( key, value, obj ) {
	obj[key] = value;
}

function show( where, obj ) {
	let code = where.querySelector('code');
	code.innerHTML = null;
	for ( let key in obj ){
		code.innerHTML += key + ': ' + obj[key] + '<br />';
	}
}


show( f__createdObj, firstObj );
show( s__createdObj, secondObj );



f__fsend.addEventListener( 'click', function() { 
	add_to_obj( f__key.value, f__value.value, firstObj )
	show( f__createdObj, firstObj );
});

s__fsend.addEventListener( 'click' , function() { 
	add_to_obj( s__key.value, s__value.value, secondObj )
	show( s__createdObj, secondObj );
});

combine.onclick = function(){
	let target = {};
	Object.assign( target, firstObj, secondObj );
	show( result, target );
}